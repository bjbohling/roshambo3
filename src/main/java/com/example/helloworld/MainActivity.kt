package com.example.helloworld

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.util.Log
import android.view.View
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        getSupportActionBar()?.setTitle("Roshambo!")

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

    }

    fun playRPS(weaponChoice: Char): String {
        //playRPS will make a random number to decide the computers play
        //if it is 1 the player will win, if it is 2, the player will tie,
        //if it is 3 the player will lose the function will return a
        //custom string with both the computer and players selection

        val compChoice = (1..3).random()
        if(weaponChoice == 'r'){
            if(compChoice == 1){
                return "Rock beats scissors, you win!"
            }
            else if(compChoice == 2){
                return "You both chose Rock, it's a tie!"
            }
            else if(compChoice == 3){
                return "Paper beats Rock, you lose!"
            }
        }
        if(weaponChoice == 'p'){
            if(compChoice == 1){
                return "Paper beats Rock, you win!"
            }
            else if(compChoice == 2){
                return "You both chose Paper, it's a tie!"
            }
            else if(compChoice == 3){
                return "Scissors beats Paper, you lose!"
            }
        }
        if(weaponChoice == 's'){
            if(compChoice == 1){
                return "Scissors beats Paper, you win!"
            }
            else if(compChoice == 2){
                return "You both chose Scissors, it's a tie!"
            }
            else if(compChoice == 3){
                return "Rock beats Scissors, you lose!"
            }
        }
        return "Select Rock, Paper or Scissors!";
    }

        fun rockClick(v: View){
             textView2.text = playRPS('r');
             Log.i("info", "The user clicked the top button")
            //If Rock is clicked, pass the coice to the play RPS function
        }

        fun paperClick(v: View) {
            textView2.text = playRPS('p');
            Log.i("info", "The user clicked the bottom button")
            //If Paper is clicked, pass the choice to the play RPS function

        }

        fun scissorsClick(v: View) {
            textView2.text = playRPS('s');
            Log.i("info", "The user clicked the Scisssors button")
            //If Scissors is clicked, pass the choice to the play RPS function

        }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
